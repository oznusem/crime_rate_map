package com.oznusem.close5exercise.Network;

import com.oznusem.close5exercise.Models.Crime;

import java.util.List;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Oz Nusem on 1/9/16.
 */
public class CL5RestProvider {

    public interface CL5RestProviderCallbacks<T> {
        void onSuccess(T item);
        void onFailure();
    }

    public static void getCrimePointsAsync(String from, String to, final CL5RestProviderCallbacks<List<Crime>> callbacks) {

        String between = String.format(Locale.US, "date between '%s' and '%s'", from, to);

        Call<List<Crime>> cl5CrimePoints  = CL5NetworkProvider.getInstance().getCL5CrimePointsService().listCL5CrimeLocation(between);
        cl5CrimePoints.enqueue(new Callback<List<Crime>>() {
            @Override
            public void onResponse(Response<List<Crime>> response, Retrofit retrofit) {
                callbacks.onSuccess(response.body());
            }

            @Override
            public void onFailure(Throwable t) {
                callbacks.onFailure();
            }
        });
    }

}
