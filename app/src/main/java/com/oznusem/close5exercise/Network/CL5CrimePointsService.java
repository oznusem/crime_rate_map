package com.oznusem.close5exercise.Network;


import com.oznusem.close5exercise.Models.Crime;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Oz Nusem on 1/9/16.
 */
public interface CL5CrimePointsService {

    @GET("/resource/cuks-n6tp.json")
    Call<List<Crime>> listCL5CrimeLocation(@Query("$where") String between);

}
