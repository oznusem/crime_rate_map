package com.oznusem.close5exercise.Models;

import java.util.List;

/**
 * Created by Oz Nusem on 1/9/16.
 */
public class Location {

    private List<Double> coordinates;

    public double getLatitude() {

        if (coordinates == null || coordinates.size() < 2) {
            return 0;
        }
        return coordinates.get(1);
    }

    public double getLongitude() {

        if (coordinates == null || coordinates.size() < 2) {
            return 0;
        }
        return coordinates.get(0);
    }

}
