package com.oznusem.close5exercise.Logic;

import com.oznusem.close5exercise.Models.Crime;
import com.oznusem.close5exercise.Models.District;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Created by Oz Nusem on 1/10/16.
 */
public class CL5CrimeDataLogic {

    public static HashMap<String,District> classifyCrimesToDistricts(List<Crime> crimes) {

        HashMap<String,District> districtHashMap = new HashMap<>();
        PriorityQueue<District> districtPriorityQueue = new PriorityQueue<>(10, new Comparator<District>() {
            @Override
            public int compare(District lhs, District rhs) {
                if (lhs.getNumberOfCrimes() == rhs.getNumberOfCrimes()) {
                    return 0;
                }
                return lhs.getNumberOfCrimes() > rhs.getNumberOfCrimes() ? -1 : 1;
            }
        });

        for (Crime crime : crimes) {

            if (!districtHashMap.containsKey(crime.getPddistrict())) {
                districtHashMap.put(crime.getPddistrict(),new District(crime.getPddistrict(),crime.getLocation()));
            }

            districtHashMap.get(crime.getPddistrict()).increaseCrimeCount();
        }

        districtPriorityQueue.addAll(districtHashMap.values());
        int crimeRate = 0;

        while (!districtPriorityQueue.isEmpty()) {

            District district = districtPriorityQueue.poll();
            if (crimeRate < 7) {
                district.setOrder(crimeRate);
            } else {
                district.setOrder(7);
            }

            crimeRate++;
        }
        return districtHashMap;
    }

}
