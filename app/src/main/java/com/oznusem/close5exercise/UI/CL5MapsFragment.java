package com.oznusem.close5exercise.UI;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.oznusem.close5exercise.Logic.CL5CrimeDataLogic;
import com.oznusem.close5exercise.Logic.CL5GoogleMapsLogic;
import com.oznusem.close5exercise.Misc.CL5TimeUtils;
import com.oznusem.close5exercise.Models.Crime;
import com.oznusem.close5exercise.Models.District;
import com.oznusem.close5exercise.Network.CL5RestProvider;
import com.oznusem.close5exercise.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Oz Nusem on 1/9/16.
 */
public class CL5MapsFragment extends SupportMapFragment implements OnMapReadyCallback {

    private static final int START_ZOOM = 12;
    //San Fransisco
    private final static Double START_LAT = 37.775951;
    private final static Double START_LON = -122.419240;
    private static final String MAP_POPULATED = "MAP_POPULATED";

    private boolean mapPopulated = false;
    private GoogleMap mMap;
    private List<Crime> mPoints;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            mapPopulated = savedInstanceState.getBoolean(MAP_POPULATED);
        }

        fetchCrimePoints();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(MAP_POPULATED,mapPopulated);
    }


    private void fetchCrimePoints() {
        CL5RestProvider.getCrimePointsAsync(CL5TimeUtils.getMonthsAgoTimeISO01(1),
                CL5TimeUtils.getCurrentTimeISO8601()
                ,new CL5RestProvider.CL5RestProviderCallbacks<List<Crime>>() {
            @Override
            public void onSuccess(List<Crime> item) {
                processPoints(item);
            }

            @Override
            public void onFailure() {
                if (!isDetached() && getView() != null) {
                    Snackbar.make(getView(),
                            getActivity().getString(R.string.failure_message), Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    private void processPoints(List<Crime> points) {

        if (points == null) {
            return;
        }

        if (mMap == null) {
            mPoints = points;
            return;
        }
        mapPopulated = true;

        HashMap<String,District> districtHashMap =  CL5CrimeDataLogic.classifyCrimesToDistricts(points);
        CL5GoogleMapsLogic.markDistrictOnMap(districtHashMap.values(),mMap);

        CL5GoogleMapsLogic.animateCameraToPoint(START_LAT,START_LON,START_ZOOM,mMap);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (!mapPopulated && mPoints != null) {
            processPoints(mPoints);
        }
    }
}
